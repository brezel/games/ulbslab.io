Plateau
=======

Il n'y a pas de taille définie de plateau (tout dépend de la place que vous avez chez vous et du nombre de factions qui
doivent jouer ensemble).

N'oubliez pas que l'espace entre deux points du plateau vaux presque 450m (463m en réalité).

Un plateau de 20 points par 20 ferait en réalité 9,2km sur 9,2. Si vous espacez chaque point par 2cm, votre plateau
fera 38cm par 38 sans compter les bordures. Une telle taille occupe déjà bien une table de cuisine.

Comment réaliser le plateau
---------------------------

Il y a autant de manières de faire que de personnes, mais voici quelques idées (de la réalisation la plus simple à la
plus complexe) :

.. note::

    Tout est réalisable en A4 ou en A3, suivant votre budget et le matériel que vous pouvez utiliser,
    Le A3 à l'avantage d'être suffisant pour une petite carte.

Plateau blanc, facile, rapide et pas chere
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Scotchez des feuilles blanches entre elles et tracez les points à la main
- Imprimez un gabarit que vous aurez fait à l'ordinateur sur plusieurs pages et scotchez les

Plateau amélioré
~~~~~~~~~~~~~~~~

- Scotchez des pochettes transparentes entre elles, imprimez un fond de carte (dessin, photo aérienne, …), vous pouvez soit mettre les points sur les pochettes ou les intégrer dans vos impressions

Plateaux avancés
~~~~~~~~~~~~~~~~

1. Utiliser une plaque de plastique transparent ou de plexiglas pas trop épais
2. Marquer les points (se procurer un appareil de gravure sur verre)
3. Ajouter des pieds pour surelever le plateau  de 5cm
4. Imprimer une vue aerienne (en faisant un montage avec https://www.geoportail.gouv.fr/donnees/photographies-aeriennes par exemple) et la placer en dessous.

Les plus personnes les plus « artistes » peuvent même refaire des carte en leur donnant un côté « ancien ».
